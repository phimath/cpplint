include(ExternalProject)
find_package(PythonInterp)

# Get cpplint
ExternalProject_Add(
        cpplint
        PREFIX "vendor/phimath"
        GIT_REPOSITORY "https://gitlab.com/phimath/cpplint.git"
        GIT_TAG v1.0
        TIMEOUT 10
        CONFIGURE_COMMAND ""
        BUILD_COMMAND ""
        INSTALL_COMMAND ""
        UPDATE_COMMAND ""
)

# CppLint script path
ExternalProject_Get_Property(cpplint source_dir)
set(CPPLINT_SCRIPT_PATH "${source_dir}/cpplint.py")

# Add target to lint the given source files
function(add_cpplint_target TARGET INPUT)
    if (NOT PYTHONINTERP_FOUND)
        message(FATAL_ERROR "CppLint requires a python interpreter")
    endif ()
    if (NOT INPUT)
        add_custom_target(${TARGET})
        return()
    endif ()

    # Remove duplicates & sort
    list(REMOVE_DUPLICATES INPUT)
    list(SORT INPUT)

    # Add target
    add_custom_target(${TARGET}
            COMMAND ${CMAKE_COMMAND} -E chdir ${CMAKE_SOURCE_DIR}
            ${PYTHON_EXECUTABLE}
            ${CPPLINT_SCRIPT_PATH}
            "--counting=detailed"
            "--extensions=cc,h"
            ${INPUT}
            DEPENDS cpplint
            COMMENT "Running ${TARGET}"
            VERBATIM)
endfunction()
